console.log("Hello World")
/*
	JS can also command our browsers to perform different arithmetic operations just like how mathematics work	
*/

// ARITHMETIC OPERATORS

// creation of variables to used in mathematics operations
let x=1397;
let y=7831;

let sum=x+y;
console.log(sum);

let difference=x-y;
console.log(difference);

let product=x*y;
console.log(product);

let quotient=x/y;
console.log(quotient);

/*
	basic operators:
		+ addition
		- subtraction
		* multiplication
		/ division
		% modulo/remainders
*/

let remainder = y % x;
console.log(remainder);

// Assignment operator
// "=" assignment operator and it is used to assign a value to a variable; the value on the right side of the operator is assigned to the left variable

let assignmentNumber=8;
/*assignmentNumber=assignmentNumber +2;
console.log(assignmentNumber);*/

// addition assignment operator
// assignmentNumber = assignmentNumber +2
// short hand for addition assignment (+=)

assignmentNumber+=2;
console.log(assignmentNumber)

// subtraction assignment operator
assignmentNumber-=2;
console.log(assignmentNumber)

// multiplication assignment operator
assignmentNumber*=2;
console.log(assignmentNumber)

// division assignment operator
assignmentNumber/=2;
console.log(assignmentNumber)

// multiple operators and parenthesis
// when multiple operators are present in a single statement, it follows PEMDAS (Parenthesis, Exponent, Multiplication, Division, Addition, Subtraction)
let mdas=1+2-3*4/5;
console.log(mdas);

/*
	MDAS
	3*4 - 12
	12/5 - 2.4
	1+2 - 3
	3-2.4 = .6
*/

let pemdas=1+(2-3)*4/5;
console.log(pemdas);

/*
	PEMDAS
	2-3 - -1
	-1*4 - -4
	-4/5 - -.8
	-.8+1 - .2
*/

// adding another set of parenthesis to create a more complex computation would still follow the same rule
let pemdas1=(1+(2-3))*(4/5);
console.log(pemdas1);

/*
	PEMDAS1
	4/5 - .8
	2-3 - -1
	1+-1 - 0
	0*.8 - 0
*/


// increments and decrements section
// assigning a value to a variable to be used in increment and decrement section
let z=1;
/*
	increment ++ adds 1 to the value of the variable whether before or after assigning of value
		pre-increment - adding 1 to the value before it is assigned to the variable
		post-increment - adding 1 to the value after it is assigned to the variable
*/
let increment = ++z;
console.log("Result of pre-increment: "+increment);
console.log("Result of pre-increment: "+z);
// the value of z is added by a value of 1 before returning the value and storing inside the variable
// the value of z was also increased by 1 even though we didnt explicitly specify any value reassignment

increment=z++;
// the value of z is at 2 before incremented
console.log("Result of post-increment: "+increment);
// the value of z was increased again reassigning the value to 3
console.log("Result of post-increment: "+z);

/*
	decrement -- subtracting 1 to the value whether before or after assigning it to the variable
		pre-decrement is subtracting 1 to the value before it is assigned to variable
		post-decrement is subtracting 1 to the value after it is assigned to the variable
*/

let decrement=--z;
// the value of z is at 3 before it was decremented
console.log("Result of pre-decrement: "+decrement);
// the value of z was reassigned to 2
console.log("Result of pre-decrement: "+z);

decrement=z--;
// the value of z was 2 before it was decremented
console.log("Result of pre-decrement: "+decrement);
// the value of z was decreased and reassigned to 1
console.log("Result of pre-decrement: "+z);

// Type Coercion
/*
	is the automatic or implicit conversion of values from one data type to another
	this happens when operations are performed on different data types that would normally
	not be possible and yield irregular result
*/

let numbA='10';
let numbB=12;
/*
	resulting data type is a string
	the value numbA, although it is technically a number, since it is a string data type, it cannot be included in any mathematical operation
*/
let coercion = numbA+numbB;
console.log(coercion);

/*
	MINI ACTIVITY
		try to have a type coercion for the following:

		- number data + number data = int/number
		- boolean + number = int/number
		- boolean + string = string
*/

let numbC =16;
let numbD=14;
let nonCoercion=numbC+numbD;
console.log(nonCoercion);
// non coercion happens when the resulting data type is not really different from both of the original data types
console.log(typeof nonCoercion);

let numbE=true+1;
console.log(numbE);
console.log(typeof numbE);

let varA="string plus "+true;
console.log(varA);;
console.log(typeof varA);

/*boolean is just like binary in javascript
	true=1
	false=0
*/


// Comparison Operators

// equality operators
/*
	checks whether the operands are equal/have the same content
	attempts to convert and compare operands of different data types
	returns boolean value
*/
console.log(1==1);
console.log(1==2);

console.log(1=="1");
console.log(1==true);
console.log('juan'=='juan');
let juan='juan';
console.log('juan'==juan);

// inequality operators
/*
	
*/
console.log(1!=1);
console.log(1!=2);
console.log(1!="1");
console.log(0!=false);
console.log('juan'!='juan');
console.log('juan'!=juan);

// Strict equality/inequality operators
/*
	checks the content of the operands
	it also checks/compares the data types of the 2 operands
	JS is loosely typed languages, meaning that the values of different data type can be stored inside a variable

	strict operators are better to be used in most cases to ensure that the data types provided are correct.
*/
// strict equality
console.log(1===1);
console.log(1===2);
console.log(1==="1");
console.log(0===false);
console.log('juan'==='juan');
console.log('juan'===juan);
// strict equality
console.log(1!==1);
console.log(1!==2);
console.log(1!=="1");
console.log(0!==false);
console.log('juan'!=='juan');
console.log('juan'!==juan);


// Relational operators
// some comparison operators check whether one value is greater or less than the other value
// just like equality and inequality operators, they return a boolean based as the assessment of the two values

let a=50;
let b =65;

let greaterThan = a>b;

let lessThan = a<b;

let greaterThanOrEqualTo = a>=b;
let lessThanOrEqualTo = a<=b;

console.log(greaterThan);
console.log(lessThan);
console.log(greaterThanOrEqualTo);
console.log(lessThanOrEqualTo);

let numStr="30";
console.log(a>numStr);
// true - product of a forced coercion to change the string into a number data type
let string="twenty";
console.log(b>=string);
// false - since the string is not numeric, the string was coverted into a number and it resulted into NaN (not a number)
// NaN - Not a Number - is the result of unsuccessful conversion of string into number data type of an alphanumeric string.

// logical operators
/*
	checking whether the values of two or more variables are true or false
*/
let isLegalAge=true;
let isRegistered=false;

// And Operator (&&)
// returns true if all values are true
let allRequirementsMet=isLegalAge && isRegistered;
console.log("Result of And Operator: "+ allRequirementsMet);

/*
	1		    	2			end result
	true         true      true
	true         false    false
	false       true      false
	false       flase     flase
*/


// OR operator

let someRequirementsMet=isLegalAge||isRegistered;
console.log("Result of OR Operator: "+ someRequirementsMet);

/*
     1          2          end result
     true     true     true
     true     false    true
     false   true     true
     false   false   false
*/


// Not Operator
// returns the opposite of the value


let someRequirementsNotMet= !isRegistered;
console.log("Result of Not Operator: "+someRequirementsNotMet)